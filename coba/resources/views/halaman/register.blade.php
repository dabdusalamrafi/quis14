<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<h1>Buat Account Baru!</h1>
<body>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First namu :</label><br><br>
        <input type="text" name="nama1"><br><br>  
        <label>Last name :</label><br><br>
        <input type="text" name="nama2"><br>

        <p>Gender:</p>
        <input type="radio"> Male <br>
        <input type="radio"> Female<br>
        <input type="radio"> Other<br>
        <label>Nationality:</label><br><br>
        <select name="negara">
            <option value="negaraindo">Indonesia</option>
            <option value="negaraamerika">Amerika</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English<br>
        <input type="checkbox"> Other<br>
        <p>Bio:</p>
        <textarea name="bio" cols="25" rows="10"></textarea><br>
      
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>